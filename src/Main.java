public class Main {

    public static void main(String[] args) {
        PrintStuff p = new PrintStuff();
        Thread t1 = new Thread () {
            public void run() {
                p.printOut("Hello KSHRD!", 250);
                p.printOut("**********************************", 250);
                p.printOut("I will try my best here at KSHRD!", 250);
                p.printOut("----------------------------------", 250);
                p.printOutNoSpace("Downloading", 1);
                p.printOutNoSpace(".........", 230);
                p.printOutNoSpace("Completed 100%!", 1);
            }
        };

        t1.run();
    }
}
