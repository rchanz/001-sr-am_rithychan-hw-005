public class PrintStuff extends Thread {
    public synchronized void printOut(String s, long speed) {
        for ( int i= 0; i < s.length(); i++) {
            // loop each character of the string obj

            System.out.print(s.charAt(i));
            try {
                Thread.sleep(speed); //set print speed
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        System.out.println(""); // finish with a new line
    }

    public synchronized void printOutNoSpace(String s, long speed) {
        for ( int i= 0; i < s.length(); i++) {
            // loop each character of the string obj

            System.out.print(s.charAt(i));
            try {
                Thread.sleep(speed); //set print speed
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        System.out.print(""); // finish with a new line
    }
}
